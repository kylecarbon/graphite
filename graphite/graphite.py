#! /usr/bin/env python3
"""
MIT License

Copyright (c) 2018 Kyle Carbon

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import click
from .src import *

CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"], max_content_width=100)


def create_memo(filename):
    print("!!not yet supported!!")


@click.command(context_settings=CONTEXT_SETTINGS)
@click.option("-t", "--template", help="Create a new template (e.g. notebook, memo, etc.).", metavar="<template>")
@click.option("-s", "--subfile", help="Create a new subfile for an existing document.", metavar="<subfile>")
@click.option("-f", "--filename", help="Write to <filename>.", metavar="<filename>")
@click.option("-i", "--install", help="Install snippets.", metavar="<install>", is_flag=True)
def cli(template, subfile, filename, install):
    """
    This creates a LaTeX template in the current directory. Current TEMPLATEs include:\n
        - notebook:     full note template, with subdirectories.\n
        - memo:         short memo template (~1 page)\n
    """
    if template:
        create_template(filename, template)
    if subfile:
        create_subfile(filename, subfile)
    if install:
        install_sublime_snippets()


if __name__ == "__main__":
    cli()
