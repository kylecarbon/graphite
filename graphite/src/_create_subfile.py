#! /usr/bin/env python3
"""
MIT License

Copyright (c) 2018 Kyle Carbon

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from pathlib import Path
import warnings
from ._utilities import *


def create_subfile(filename, subfilename):
    if not filename:
        raise ValueError("filename cannot be empty.")
    if not subfilename:
        raise ValueError("subfilename cannot be empty.")

    # make sure we have the correct file extension
    if not filename.endswith(".tex"):
        filename += ".tex"
    if not subfilename.endswith(".tex"):
        subfilename += ".tex"

    rel_dirs = find_relative_directory(filename)
    if not rel_dirs:
        warnings.warn("Could not find <{}> on current path.".format(filename))
        return None

    subfile_text = get_subfile_str()
    subfile_text = subfile_text.format(rel_dirs + filename)
    subfile_path = Path.cwd() / subfilename
    prompt_str = "New subfile: " + str(subfile_path) + "\nIs this correct?"
    answer = query_yes_no(prompt_str)
    if answer:
        subfile_handle = subfile_path.open(mode="w")
        subfile_handle.write(subfile_text)
        subfile_handle.close()
        print("Created file: {}".format(str(subfile_path)))
        print("Don't forget to add this to your LaTeX document somewhere!")


def find_relative_directory(filename):
    # variables to make sure we stay in /home/$USER
    cwd = Path.cwd()
    home_user_dir = Path.home()
    # variable to keep track of relative directories from cwd to directory where filename is found
    rel_dirs = ""
    while True:
        # make sure we stay in /home/$USER
        if home_user_dir not in cwd.parents:
            return None

        # check directory for filename
        filepath_to_check = cwd / filename
        if filepath_to_check.is_file():
            break
        else:
            # go up one directory
            cwd = cwd / ".."
            cwd = cwd.resolve()
            rel_dirs += "../"

    return rel_dirs


def get_subfile_str():
    subfile_text = """\documentclass[{}]{{subfiles}}

\\begin{{document}}

\\end{{document}}
"""
    return subfile_text
