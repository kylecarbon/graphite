#! /usr/bin/env python3
"""
MIT License

Copyright (c) 2018 Kyle Carbon

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from fuzzywuzzy import fuzz, process
from operator import itemgetter
from .templates import *
from ._utilities import *

def create_template(filename, template):
    """
    @brief      Create a directory with a LaTeX template with the given filename.

    @param      filename  string - filename (can be empty/None)
    @param      template  string - choice of template

    @return     None.
    """
    templates = {"simplenote": 0, "notebook": 0, "article": 0, "memo": 0}
    template_keys = "\n"
    for key in templates:
        template_keys += "\t" + key + "\n"

    while True:
        best_match = get_best_string_match(template, templates)
        prompt_str = "Best match: " + best_match + ". Is this correct?"
        answer = query_yes_no(prompt_str)
        if answer:
            break
        else:
            print("Please select one of: " + str(template_keys))
            template = input()

    if best_match == "simplenote":
        create_simplenote(filename)
    elif best_match == "notebook":
        create_notebook(filename)
    elif best_match == "article":
        create_article(filename)
    elif best_match == "memo":
        create_memo(filename)
