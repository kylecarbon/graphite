#! /usr/bin/env python3
"""
MIT License

Copyright (c) 2018 Kyle Carbon

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
from fuzzywuzzy import fuzz, process
from operator import itemgetter
from pathlib import Path
import shutil
import pip


def query_yes_no(question, default="yes"):
    """
    Ask a yes/no question via input() and return their answer.

    @param      question  A string that is presented to the user. "default" is the presumed answer if the user just hits
                          <Enter>. It must be "yes" (the default), "no" or None (meaning an answer is required of the
                          user).
    @param      default   The default prompt.

    @return     Return True if "answer" is valid/yes.
    """
    valid = {"yes": True, "y": True, "ye": True, "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        print(question + prompt)
        choice = input().lower()
        if default is not None and choice == "":
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            print("Please respond with 'yes' or 'no' " "(or 'y' or 'n').\n")


def get_best_string_match(user_choice, options):
    for option in options:
        ratio = fuzz.ratio(user_choice, option)
        options[option] = ratio

    return max(options.items(), key=itemgetter(1))[0]


def install_sublime_snippets():
    cwd = Path.cwd()
    src = cwd / "snippets/sublime"
    print("INSTALLING SNIPPETS")
    print(src)
    a = pip.main(["show", "graphite"])

