#! /usr/bin/env python3
"""
MIT License

Copyright (c) 2018 Kyle Carbon

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from ._template_utilities import *

from pathlib import Path


##
# @brief      Creates a carbonarticle.
##
# @param      memo_name  [optional] name of memo
##
def create_memo(memo_name):

    # make directory
    cwd = Path.cwd()
    if not memo_name:
        memo_name = "memo"
    note_dir = cwd / memo_name
    note_dir.mkdir(parents=True, exist_ok=True)

    # setup options for main file
    memo_name_out = memo_name + ".tex"
    template_name = "templates"
    file_path = note_dir / memo_name_out
    f_handle = file_path.open(mode="w")
    options = {
        "document_class": "article",
        "memo_name": memo_name,
        "main_font": "Roboto",
        "mono_font": "DejaVu Sans Mono",
        "path_to_formatting": template_name + "/formatting",
        "path_to_packages": template_name + "/packages",
        "path_to_pagestyle": template_name + "/pagestyle",
        "path_to_preamble": template_name + "/preamble",
    }
    # get and format main file string
    mainfile_template_string = main_file_template()
    f_handle.write(mainfile_template_string.format(**options))
    f_handle.close()

    # make subdirectory for sections
    template_dir = note_dir / template_name
    template_dir.mkdir(parents=True, exist_ok=True)

    # add formatting into subdirectory
    formatting_string = "formatting.tex"
    formatting_path = template_dir / formatting_string
    formatting_template_string = formatting_template()
    formatting_handle = formatting_path.open(mode="w")
    formatting_handle.write(formatting_template_string.format(memo_name))
    formatting_handle.close()

    pagestyle_string = "pagestyle.tex"
    pagestyle_path = template_dir / pagestyle_string
    pagestyle_template_string = pagestyle_template()
    pagestyle_handle = pagestyle_path.open(mode="w")
    pagestyle_handle.write(pagestyle_template_string.format(memo_name))
    pagestyle_handle.close()

    # add packages into subdirectory
    packages_string = "packages.tex"
    packages_path = template_dir / packages_string
    packages_template_string = packages_template()
    packages_handle = packages_path.open(mode="w")
    packages_handle.write(packages_template_string.format(memo_name))
    packages_handle.close()

    # make subdirectory for figures
    figure_dir = note_dir / "figures"
    figure_dir.mkdir(parents=True, exist_ok=True)

    print("Created article in " + str(note_dir))


def main_file_template():
    main_file_string = """\documentclass{{{document_class}}}
\input{{{path_to_packages}}}
\input{{{path_to_formatting}}}
\input{{{path_to_pagestyle}}}

%% font choice
\\setmainfont{{{main_font}}}
\\setmonofont{{{mono_font}}}
%% add to graphics path
\\appendtographicspath{{figures/}}

\\begin{{document}}
\\pagestyle{{shieldPageStyle}}
\pagenumbering{{arabic}}
\setcounter{{page}}{{1}}

\\textbf{{\Large <insert title>}}, \\today \\\\
<author> \\\\

\\textbf{{Summary}} \\\\
<summary goes here>

\\end{{document}}
"""
    return main_file_string
