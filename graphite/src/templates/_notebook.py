#! /usr/bin/env python3
"""
MIT License

Copyright (c) 2018 Kyle Carbon

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from ._template_utilities import *

from pathlib import Path

##
# @brief      Creates a complete Notebook.
##
# @param      notebook_name  [optional] name of notebook
##
# @return     { description_of_the_return_value }
##
def create_notebook(notebook_name):

    # make directory
    cwd = Path.cwd()
    if not notebook_name:
        notebook_name = "notebook"
    note_dir = cwd / notebook_name
    note_dir.mkdir(parents=True, exist_ok=True)

    # setup options for main file
    notebook_name_out = notebook_name + ".tex"
    section_name = "section1"
    preamble_name = "preamble"
    frontmatter_name = "frontmatter"
    file_path = note_dir / notebook_name_out
    f_handle = file_path.open(mode="w")
    subfile_name = "{}_part1".format(notebook_name)
    subfile_relative_path = "{}/{}".format(section_name, subfile_name)
    options = {
        "document_class": "report",
        "notebook_name": notebook_name,
        "main_font": "Roboto",
        "mono_font": "DejaVu Sans Mono",
        "path_to_formatting": preamble_name + "/formatting",
        "path_to_packages": preamble_name + "/packages",
        "path_to_pagestyle": preamble_name + "/pagestyle",
        "path_to_commands": preamble_name + "/commands",
        "path_to_title": frontmatter_name + "/title",
        "path_to_toc": frontmatter_name + "/toc",
        "path_to_subfile": subfile_relative_path,
        "path_to_appendix": "appendix",
    }
    # get and format main file string
    mainfile_template_string = main_file_template()
    f_handle.write(mainfile_template_string.format(**options))
    f_handle.close()

    # make subdirectory for misc
    subdir = note_dir / section_name
    subdir.mkdir(parents=True, exist_ok=True)
    preamble_dir = note_dir / preamble_name
    preamble_dir.mkdir(parents=True, exist_ok=True)
    frontmatter_dir = note_dir / frontmatter_name
    frontmatter_dir.mkdir(parents=True, exist_ok=True)

    # add title into subdirectory
    title_string = "title.tex"
    title_path = frontmatter_dir / title_string
    title_template_string = title_template()
    title_handle = title_path.open(mode="w")
    title_handle.write(title_template_string.format(notebook_name))
    title_handle.close()

    toc_string = "toc.tex"
    toc_path = frontmatter_dir / toc_string
    toc_template_string = toc_template()
    toc_handle = toc_path.open(mode="w")
    toc_handle.write(toc_template_string.format(notebook_name))
    toc_handle.close()

    # add packages into subdirectory
    packages_string = "packages.tex"
    packages_path = preamble_dir / packages_string
    packages_template_string = packages_template()
    packages_handle = packages_path.open(mode="w")
    packages_handle.write(packages_template_string.format(notebook_name))
    packages_handle.close()

    # add formatting into subdirectory
    formatting_string = "formatting.tex"
    formatting_path = preamble_dir / formatting_string
    formatting_template_string = formatting_template()
    formatting_handle = formatting_path.open(mode="w")
    formatting_handle.write(formatting_template_string.format(notebook_name))
    formatting_handle.close()

    pagestyle_string = "pagestyle.tex"
    pagestyle_path = preamble_dir / pagestyle_string
    pagestyle_template_string = pagestyle_template()
    pagestyle_handle = pagestyle_path.open(mode="w")
    pagestyle_handle.write(pagestyle_template_string.format(notebook_name))
    pagestyle_handle.close()

    # add commands into subdirectory
    commands_string = "commands.tex"
    commands_path = preamble_dir / commands_string
    commands_template_string = commands_template()
    commands_handle = commands_path.open(mode="w")
    commands_handle.write(commands_template_string.format(notebook_name))
    commands_handle.close()

    # make subdirectory for figures
    figure_dir = note_dir / "figures"
    figure_dir.mkdir(parents=True, exist_ok=True)

    # add subfile into subdirectory
    subfile_template_string = subfile_template()
    subfile_template_string = subfile_template_string.format(notebook_name)
    subfile_name_out = subfile_name + ".tex"
    subfile_path = subdir / subfile_name_out
    subfile_handle = subfile_path.open(mode="w")
    subfile_handle.write(subfile_template_string)
    subfile_handle.close()

    # add appendix into subdirectory
    appendix_string = "appendix.tex"
    appendix_path = note_dir / appendix_string
    appendix_template_string = appendix_template()
    appendix_handle = appendix_path.open(mode="w")
    appendix_handle.write(appendix_template_string.format(notebook_name))
    appendix_handle.close()

    print("Created notebook in " + str(note_dir))


def main_file_template():
    main_file_string = """\documentclass{{{document_class}}}
\input{{{path_to_packages}}}
\input{{{path_to_formatting}}}
\input{{{path_to_pagestyle}}}
\input{{{path_to_commands}}}

%% font choice
\setmainfont{{{main_font}}}
\setmonofont{{{mono_font}}}
%% add to graphics path
\graphicspath{{figures/}}
%\\appendtographicspath{{figures/}}
%\\indexsetup{{level=\\part}}
%\\addbibresource{{<your bib>.bib}}

\\begin{{document}}

\input{{{path_to_title}}}
\input{{{path_to_toc}}}
\pagestyle{{shieldPageStyle}}

% include the first section through a subfile
\subfile{{{path_to_subfile}}}
\section{{notebook 2}}
\\begin{{mylist}}
# <insert here> \index{{sample index}}
\end{{mylist}}

\clearpage
\\printbibliography[heading=bibintoc, title={{References}}]
\clearpage
\subfile{{{path_to_appendix}}}

\printindex

\end{{document}}
"""
    return main_file_string


def subfile_template():
    subfile_string = """\documentclass[../{}.tex]{{subfiles}}

\\begin{{document}}

\section{{<Section1>}}
hello, world

\end{{document}}
"""
    return subfile_string


def title_template():
    title_string = """%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                 TITLE PAGE                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\\begin{{titlepage}}
\\title{{<DOCUMENT TITLE>}}
\\author[1]{{<YOUR NAME> \\thanks{{ \href{{mailto:<YOUR EMAIL>}}{{<YOUR EMAIL>}} }} }}
\\affil[1]{{<YOUR TITLE>}}
\\renewcommand\Authands{{ and }}

\maketitle
\\begin{{abstract}}
    <summary goes here>
\end{{abstract}}
\\vfill

\pagestyle{{empty}}
\pagenumbering{{gobble}} % turn off page numbering on title page
\end{{titlepage}}
"""
    return title_string

