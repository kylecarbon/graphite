#! /usr/bin/env python3
"""
MIT License

Copyright (c) 2018 Kyle Carbon

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


def appendix_template():
    appendix_string = """\documentclass[../{}.tex]{{subfiles}}
\\begin{{document}}

\\appendix
\section{{<Appendix example 1}}

\end{{document}}
"""
    return appendix_string


def formatting_template():
    format_template = """

\\newcommand{{\\nordZero}}{{2E3440}}
\\newcommand{{\\nordOne}}{{3B4252}}
\\newcommand{{\\nordTwo}}{{434C5E}}
\\newcommand{{\\nordThree}}{{4C566A}}
\\newcommand{{\\nordFour}}{{D8DEE9}}
\\newcommand{{\\nordFive}}{{E5E9F0}}
\\newcommand{{\\nordSix}}{{ECEFF4}}
\\newcommand{{\\nordSeven}}{{8FBCBB}}
\\newcommand{{\\nordEight}}{{88C0D0}}
\\newcommand{{\\nordNine}}{{81A1C1}}
\\newcommand{{\\nordTen}}{{5E81AC}}
\\newcommand{{\\nordEleven}}{{BF616A}}
\\newcommand{{\\nordTwelve}}{{D08770}}
\\newcommand{{\\nordThirteen}}{{EBCB8B}}
\\newcommand{{\\nordFourteen}}{{A3BE8C}}
\\newcommand{{\\nordFifteen}}{{B48EAD}}

\\newcommand{{\\nordWhite}}{{\\nordSix}}
\\newcommand{{\\nordBlack}}{{\\nordZero}}
\\newcommand{{\\nordGrey}}{{\\nordThree}}
\\newcommand{{\\nordBlue}}{{\\nordNine}}
\\newcommand{{\\nordRed}}{{\\nordEleven}}
\\newcommand{{\\nordYellow}}{{\\nordThirteen}}
\\newcommand{{\\nordOrange}}{{\\nordTwelve}}
\\newcommand{{\\nordGreen}}{{\\nordFourteen}}

\definecolor{{nord0}}{{HTML}}{{\\nordZero}}
\definecolor{{nord1}}{{HTML}}{{\\nordOne}}
\definecolor{{nord2}}{{HTML}}{{\\nordTwo}}
\definecolor{{nord3}}{{HTML}}{{\\nordThree}}
\definecolor{{nord4}}{{HTML}}{{\\nordFour}}
\definecolor{{nord5}}{{HTML}}{{\\nordFive}}
\definecolor{{nord6}}{{HTML}}{{\\nordSix}}
\definecolor{{nord7}}{{HTML}}{{\\nordSeven}}
\definecolor{{nord8}}{{HTML}}{{\\nordEight}}
\definecolor{{nord9}}{{HTML}}{{\\nordNine}}
\definecolor{{nord10}}{{HTML}}{{\\nordTen}}
\definecolor{{nord11}}{{HTML}}{{\\nordEleven}}
\definecolor{{nord12}}{{HTML}}{{\\nordTwelve}}
\definecolor{{nord13}}{{HTML}}{{\\nordThirteen}}
\definecolor{{nord14}}{{HTML}}{{\\nordFourteen}}
\definecolor{{nord15}}{{HTML}}{{\\nordFifteen}}

\definecolor{{nordblack}}{{HTML}}{{\\nordBlack}}
\definecolor{{grey}}{{HTML}}{{\\nordGrey}}
\definecolor{{nordwhite}}{{HTML}}{{\\nordWhite}}
\definecolor{{blue}}{{HTML}}{{\\nordBlue}}
\definecolor{{red}}{{HTML}}{{\\nordRed}}
\definecolor{{orange}}{{HTML}}{{\\nordOrange}}
\definecolor{{yellow}}{{HTML}}{{\\nordYellow}}
\definecolor{{green}}{{HTML}}{{\\nordGreen}}

%%%%%%%%%%%%%%%%%%%% used to reference range of equations: Eq. 1--5 %%%%%%%%%%%%%%%%%%%%
\\newcommand{{\crefrangeconjunction}}{{--}}

%%%%%%%%%%%%%%%%%%%% roman numerals in footnotes%%%%%%%%%%%%%%%%%%%%
\\renewcommand{{\\thefootnote}}{{\\roman{{footnote}}}}

%%%%%%%%%%%%%%%%%%%% Underscore interpretation %%%%%%%%%%%%%%%%%%%%
% in text/normal mode: underscores are underscores
% in math mode: underscores generate subscripts
\makeatletter
\AtBeginDocument{{
    \catcode`_=12
    \\begingroup\lccode`~=`_
    \lowercase{{\endgroup\let~}}\sb
    \mathcode`_="8000
    \immediate\write\@auxout{{\catcode`_=12 }}
}}
\makeatother

\lstdefinestyle{{my_code_style}}{{
    backgroundcolor=\color{{nord0}},
    basicstyle=\small\color{{nord6}},
    commentstyle=\small\color{{nord13}},
    keywordstyle=\color{{nord11}},
    numberstyle=\tiny\color{{nord3}},
    stringstyle=\color{{nord6}},
    breakatwhitespace=false,
    breaklines=true,
    captionpos=b,
    keepspaces=true,
    numbers=left,
    numbersep=5pt,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=2
}}
\lstset{{style=my_code_style}}
\\renewcommand\lstlistingname{{Algorithm}}
\\renewcommand\lstlistlistingname{{Algorithms}}
\def\lstlistingautorefname{{Alg.}}

%%%%%%%%%%%%%%%%%%%% Use TitleSec to format section names %%%%%%%%%%%%%%%%%%%%
% default TOC depth
\setcounter{{secnumdepth}}{{5}}
% format part
\\titleformat{{\part}}
[display]
{{\centering\\normalfont\Huge\\bfseries}}
{{\\titlerule[5pt]\\vspace{{3pt}}\MakeUppercase{{\partname}} \\thepart}}
{{0pt}}
{{\\titlerule[5pt]\\vspace{{3pt}}\huge}}
% format chapter
\\titleformat{{\chapter}}[display]{{\Huge\\bfseries}}{{Topic \\thechapter:}}{{0pt}}{{}}{{}}
% format section through subsection
\\titleformat*{{\section}}{{\huge\\bfseries}}
\\titleformat*{{\subsection}}{{\LARGE\\bfseries}}
\\titleformat*{{\subsubsection}}{{\Large\\bfseries}}
% format paragraph so it displays numbering
\\titleformat{{\paragraph}}
{{\\normalfont\large\\bfseries}}{{\\theparagraph}}{{1em}}{{}}
\\titlespacing*{{\paragraph}}
{{0pt}}{{3.25ex plus 1ex minus .2ex}}{{1.5ex plus .2ex}}
% format subparagraph so it displays numbering
\\titleformat{{\subparagraph}}
{{\\normalfont\\normalsize\\bfseries}}{{\\thesubparagraph}}{{1em}}{{}}
\\titlespacing*{{\subparagraph}}
{{0pt}}{{3.25ex plus 1ex minus .2ex}}{{1.5ex plus .2ex}}
% titlesec is a little broken on Ubuntu 16.04, so this is a work-around to get numbering for paragraphs
\\usepackage{{etoolbox}}
\makeatletter
\patchcmd{{\\ttlh@hang}}{{\parindent\z@}}{{\parindent\z@\leavevmode}}{{}}{{}}
\patchcmd{{\\ttlh@hang}}{{\\noindent}}{{}}{{}}{{}}
\makeatother
"""
    return format_template


def packages_template():
    package_template = """%---------------------- PACKAGES ----------------------%
\\usepackage[letterpaper,margin=0.75in]{{geometry}}
\\usepackage{{graphicx}}
\\usepackage{{amsmath}}
\\usepackage{{mathtools}}
\\usepackage[backend=biber, style=numeric, citestyle=numeric-comp, maxbibnames=9, maxcitenames=2, sorting=none]{{biblatex}}

%\\usepackage[backend=biber, style=numeric, citestyle=authoryear-comp, maxbibnames=9, maxcitenames=2]{{biblatex}}
\\usepackage{{parskip}} % no auto indenting
\\usepackage{{titletoc}}
\\usepackage{{titlesec}}
\\usepackage[titletoc, title, page]{{appendix}}
\\usepackage{{notoccite}} % prevents trouble with table of contents and citing references, must come before hyperref
\\usepackage{{tocbibind}}
\\usepackage{{array}}
\\usepackage{{fancyhdr}} % fancy headers & footers
\\usepackage{{subfiles}} % include subfiles for modular documents
\\usepackage{{booktabs}} % better tables?
\\usepackage{{tabu}} % definitely better tables...
\\usepackage{{multirow}}
\\usepackage{{float}}
\\usepackage{{authblk}} % better titlepage
\\usepackage[export]{{adjustbox}}
\\usepackage[font=small, labelfont=bf, justification=justified, format=plain]{{caption}}
\\usepackage{{subcaption}} % better captions for tables, figures, etc.
\\usepackage{{lastpage}}
\\usepackage{{ulem}}
\\usepackage{{todonotes}}
\\usepackage{{ctable}}
\\usepackage{{epstopdf}} % nice figures using EPS format
\\usepackage{{anyfontsize}}
\\usepackage{{datetime2}}
\DTMsetdatestyle{{iso}}
%% for colours
\\usepackage{{xcolor}}
\\usepackage{{framed,color}}

\\usepackage{{imakeidx}} % for index
\makeindex[intoc]
\makeindex[title=Index, columns=3]
\\usepackage[colorlinks = true,
			 linkcolor = nord10,
			 citecolor = nord11,
			 urlcolor = nord10]{{hyperref}}
\\usepackage{{cleveref}} % must load after hyperref

%% related to font selection
\\usepackage[bitstream-charter]{{mathdesign}} % math font
\\usepackage[UKenglish]{{babel}} % rules for grammar/hypenation/etc.
\\usepackage{{fontspec}} % use system-fonts
\\usepackage{{fontawesome}}
\\usepackage{{csquotes}}

\\usepackage[sharp]{{easylist}}
\\usepackage{{listings}} % for pseudocode

\\usepackage{{mathptmx}}
\\usepackage[multiple]{{footmisc}}
\\usepackage{{wrapfig}}

\\usepackage{{textcomp}}
\\usepackage{{gensymb}}
\\usepackage{{enumitem}}
\\setlist[itemize]{{leftmargin=*,noitemsep}}

\\usepackage[framemethod=tikz]{{mdframed}}
\\newmdenv[innerlinewidth=0.5pt, roundcorner=4pt,linecolor=lightblue,backgroundcolor=black!2,innerleftmargin=6pt,
innerrightmargin=6pt,innertopmargin=6pt,innerbottommargin=6pt]{{callbox}}
"""
    return package_template


def pagestyle_template():
    pagestyle_template = """
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               Page Styles                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\smallcompanylogo{{\includegraphics[height=0.35cm]{{shieldai_icon_monochrome.pdf}}}}
%% Define custom page style
\\fancypagestyle{{shieldPageStyle}}{{
    \\fancyhf{{}}
    \\fancyhead[L]{{\small \leftmark}}
    \\fancyhead[R]{{\small \\rightmark}}
    \\fancyfoot[C]{{{{\smallcompanylogo}}}}
    \\fancyfoot[L]{{\small Copyright \copyright{{}} Shield AI \\the\year}}
    \\fancyfoot[R]{{\small \\thepage\ of \pageref{{LastPage}}}}
    \\renewcommand{{\headrulewidth}}{{1pt}}
    \\renewcommand{{\\footrulewidth}}{{1pt}}
}}

\\fancypagestyle{{shieldTocPageStyle}}{{
    \\fancyhf{{}}
    \\fancyfoot[L]{{\small Copyright \copyright{{}} Shield AI \\the\year}}
    \\fancyfoot[C]{{{{\smallcompanylogo}}}}
    \\fancyfoot[R]{{\small \\thepage}}
    \\renewcommand{{\headrulewidth}}{{0pt}}
    \\renewcommand{{\\footrulewidth}}{{1pt}}
}}
"""
    return pagestyle_template


def toc_template():
    toc_template = """
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                    TOC                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\pagestyle{{shieldTocPageStyle}}
\pagenumbering{{roman}}
\setcounter{{page}}{{1}}
\setcounter{{tocdepth}}{{3}}

\phantomsection\\addtocontents{{toc}}{{~\hfill \\textbf{{Page}}\par}}
\\tableofcontents
\cleardoublepage\phantomsection
% \phantomsection\\addcontentsline{{toc}}{{section}}{{List of Tables}}
\listoftables
\phantomsection
\listoffigures
\phantomsection
\\addcontentsline{{toc}}{{section}}{{Algorithms}}
\lstlistoflistings
\phantomsection
\\todototoc\listoftodos

\clearpage
\pagenumbering{{arabic}}
\setcounter{{page}}{{1}}

\\renewcommand{{\subsectionmark}}[1]{{\markright{{\\thesubsection~- ~#1}}}}

\\renewcommand\lstlistingname{{Code snippet}}
\\renewcommand\lstlistlistingname{{Code snippets}}
\def\lstlistingautorefname{{CodeSnippet}}
"""
    return toc_template


def commands_template():
    commands_template = """
% deletes anything in bracketed section
\\newcommand{{\\remove}}[1]{{}}

% alias for monospaced font
\\newcommand{{\code}}[1]{{\\texttt{{#1}}}}

% appendtographicspath{{<new_path>}}: appends new_path to pre-existing figure path
%  NOTE: you must have already called `\graphicspath{{figures/}}`
\makeatletter
\\newcommand\\appendtographicspath[1]{{%
  \g@addto@macro\Ginput@path{{#1}}%
}}
\makeatother

%%%%%%%%%%%%%%%%%%%% Macros for note-taking %%%%%%%%%%%%%%%%%%%%
\\newcommand{{\mytodo}}[1]{{\\todo[inline,backgroundcolor=nord11,bordercolor=nord11]{{#1}}}}
\\newcommand{{\mycomment}}{{\mytodo}}

\\newcommand{{\mynote}}[2][Note]{{
    \definecolor{{shadecolor}}{{HTML}}{{\\nordEight}}
    \\begin{{snugshade}}
        \\textbf{{#1}} #2
    \end{{snugshade}}
}}

\\newcommand{{\mydefinition}}[1]{{
    \definecolor{{shadecolor}}{{HTML}}{{\\nordEight}}
    \\begin{{snugshade}}
        \\textbf{{Definition}} #1
    \end{{snugshade}}
}}

\\newcommand{{\mywarning}}[2][Warning]{{
    \definecolor{{shadecolor}}{{HTML}}{{\\nordRed}}
    \\begin{{snugshade}}
        \\textbf{{#1}} #2
    \end{{snugshade}}
}}

\\newcommand\myEnumerate{{\ListProperties(Space=-0.1cm,Space*=-1mm)}}
\let\OldEasylist\easylist
\let\OldEndEasylist\endeasylist
\\newenvironment{{myenumerate}}{{
    \OldEasylist[enumerate]
    \ListProperties(Space=-0.2cm,
                    Space*=-2mm)%
}}{{
    \OldEndEasylist
}}

\\newenvironment{{mylist}}{{
    \OldEasylist[itemize]
    \ListProperties(Space=-0.2cm,
                    Space*=-2mm)%
}}{{
    \OldEndEasylist
}}
"""
    return commands_template
