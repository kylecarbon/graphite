# Overview
Command-line-interface (CLI) to help write LaTeX efficiently.
# Setup
## Dependencies
Incomplete list:
* LaTeX (find one for your distribution)
  * [`subfiles`](https://ctan.org/pkg/subfiles?lang=en)
  * [`todonotes`](https://ctan.org/pkg/todonotes?lang=en)
* `lualatex` (only tested with version `beta-0.80.0`)

## Installation
```
git clone https://gitlab.com/kylecarbon/graphite.git
pip3 install -e graphite
```

# Usage
```
graphite -h
```

## Examples
#### Create a notebook
```
cd <directory>
graphite -f <notebook_name> -t notebook
```
This creates a new folder in `directory/notebook_name`. In it, you will find the following layout:
```
<notebook_name>
    figures
    sections
    templates
    <notebook_name>.tex
```
A couple important notes:
* `figures` will already be in LaTeX's graphics path.
* `sections` should be able to contain arbitrarily nested sections of your LaTeX document, although I've only gone three levels deep.
* `template` will contain some package setup and formatting defaults. Feel free to edit & tweak but you can avoid this directory if you want.

##### Adding a subfile to a notebook
```
cd <subdirectory of your choice>
graphite -f <main document name> -s <new subfile name>
```
Quick notes:
* it allows you to link any subfile to a master LaTeX document
* the subfile itself must be included _somewhere_ on the LaTeX path -- this can be either in the master document itself or in another subfile that is already included in the master document
  * in a file somewhere, include: `\subfile{<path to subfile>}`
  * this is left up to the user to include correctly


#### Create a single-page memo
```
cd <directory>
graphite -f <memo_name> -t memo
```
This creates a new folder in `directory/memo_name`. In it, you will find the following layout:
```
<memo_name>
    figures
    <memo_name>.tex
```

## Tips
#### LaTeX snippets
Configure your editor with LaTeX snippets for commonly used commands. [`graphite/src/snippets`](https://gitlab.com/kylecarbon/graphite/tree/master/graphite/src/snippets) includes snippets for:
* sublime
* vscode

The list of snippets includes:
* generating:
  * tables
  * figures
  * subfigures
  * bullet & enumerated lists
  * code snippets
* adding TODO notes (these get tracked in the TOC)
##### Quick lists
```
\begin{mylist}
# <insert note here>
## <sub-bullet>
\end{mylist}
```
##### Quick tables
```
\begin{table}[htb!]
    \centering
    \begin{tabular}{l c}
        \specialrule{.3em}{.2em}{.2em}
        <col1> & <col2> \\
        \toprule
        row1 & XXX \\
        row2 & XXX \\
        row3 & XXX \\
        \specialrule{.3em}{.2em}{.2em}
    \end{tabular}
    \caption[<short caption>]{<long caption>}
    \label{table:table_name}
\end{table}
```
##### Quick code snippets
```
\begin{lstlisting}[language=<insert language>, caption={}, label={}]
<insert here>
\end{lstlisting}
```
##### TODOs
`carbontex` uses the [`todonotes` package](https://ctan.org/pkg/todonotes?lang=en) to create the `\mytodo` macro:
```
\mytodo{<insert TODO>}
```